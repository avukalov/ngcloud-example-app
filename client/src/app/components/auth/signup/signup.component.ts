import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, AbstractControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  maxDate;

  signupForm = this.fb.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    email: ['', Validators.required],
    passwordForm: this.fb.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['']
    }),
    birthdate: ['', Validators.required],
    agree: ['', Validators.required]
  });
  

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
  }

  // signupForm = new FormGroup({
  //   firstName: new FormControl('', Validators.required),
  //   lastName: new FormControl('', Validators.required),
  //   email: new FormControl('', Validators.required),
  //   password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  //   confirmPassword: new FormControl('', [Validators.required]),
  //   birthdate: new FormControl('', Validators.required),
  //   agree: new FormControl('', Validators.required)
  // });

  // signupForm ? 

  onSubmit(){
    console.log(this.signupForm.value);
    
  }

  checkPasswordValidator(formGroup: FormGroup){
    let password = formGroup.get('password').value;
    let confirmPassword = formGroup.get('confirmPassword').value;

    return password === confirmPassword ? null : { notsame: true };
  }

}
